# SAM POC with Docker and Graylog
This is a Gitlab Repository to do an easy Demo or POC Setup with Graylog2 ( https://www.graylog.org/ ) Open Source Software.

To do the POC on your Local Machine the following Steps are necessary:

* Install Docker (https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/#install-using-the-repository)
  * curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
  * sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
 * sudo apt-get update
 * sudo apt-get install docker-ce
* Install Docker-Compose (https://docs.docker.com/compose/install/)
  * sudo pip install docker-compose
* Clone this Gitlab Repository
  * git clone https://gitlab.com/rstumpner/sam-poc-docker-graylog2
* Change Directory to the Repository
  * cd sam-poc-docker-graylog2
* Fire up docker-compose up
  * docker-compose up
* Start the Browser and type in http://localhost:9000
  * Login with
    * Username: admin
    * Password: admin 
* Logging Ports in Docker Image
  * 514 UDP Syslog
  * 514 TCP Syslog
  * add Ports in Docker Compose File (docker-compose.yml)
